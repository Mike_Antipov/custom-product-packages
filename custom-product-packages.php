<?php
/**
 * Plugin Name: Custom Product Packages For WooCommerce
 * Description: Use different products and subscriptions as sub-options for a product
 * Version: 1.0
 * Author: Mike Antipov
 * WC requires at least: 3.0.0
 * WC tested up to: 5.7
 */
defined( 'ABSPATH' ) || exit ();

/**
 * Override WC variable product template
 */

if ( ! function_exists( 'woocommerce_variable_add_to_cart' ) ) {
	/**
	 * Output the variable product add to cart area.
	 */
	function woocommerce_variable_add_to_cart() {
		global $product;
		
		// Enqueue variation scripts.
		wp_enqueue_script( 'wc-add-to-cart-variation' );

		// Get Available variations?
		$get_variations = count( $product->get_children() ) <= apply_filters( 'woocommerce_ajax_variation_threshold', 30, $product );

		// Get product ID
		$cat = strip_tags(wc_get_product_category_list($product->get_id()));
		preg_match('/Digital Wayfinding/', $cat, $cat);
		
		if(!empty($cat[0]) && $cat[0] == 'Digital Wayfinding') {
			// Load MVG custom template, add custom endpoint
			$template_path = WP_PLUGIN_DIR . '/custom-product-packages/templates/mvg.php';
			
			// Load scrits and add a custom endpoint
			wp_enqueue_style( 'cpp-css', plugin_dir_url( __FILE__ ) . 'assets/css/cpp.css');
			wp_enqueue_script( 'cpp-js', plugin_dir_url( __FILE__ ) . 'assets/js/cpp.js', '', 1, true);

			wc_get_template(
				'',
				array(
					'available_variations' => $get_variations ? $product->get_available_variations() : false,
					'attributes'           => $product->get_variation_attributes(),
					'selected_attributes'  => $product->get_default_attributes(),
				),
				'',
				$template_path
			);
		} else {
			// Load default template
			wc_get_template(
				'single-product/add-to-cart/variable.php',
				array(
					'available_variations' => $get_variations ? $product->get_available_variations() : false,
					'attributes'           => $product->get_variation_attributes(),
					'selected_attributes'  => $product->get_default_attributes(),
				)
			);
		}
	}
}

/**
 * Add AJAX add to cart batch function
 */
function add_to_cart_batch(){
	header('Content-type: application/json');
	$result = '';
	foreach ($_POST['tmpCart'] as $product) {
		$id = $product[id];
		$qty = $product[qty];
		$result .= WC()->cart->add_to_cart($id, $qty) . ' has been added, ' . 'ID of product is ' . $id;
	}
	echo json_encode('Result is ' . $result);
}
add_action('wc_ajax_add_to_cart_batch','add_to_cart_batch');