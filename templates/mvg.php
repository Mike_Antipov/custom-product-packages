<?php
/**
 * Template Name: MVG Unit
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Begin custom product loading
 */
require WP_PLUGIN_DIR . '/custom-product-packages/vendor/autoload.php';

use Automattic\ WooCommerce\ Client;

$http = IS_LOCAL ? 'http' : 'https';
$woocommerce = new Client(
    $http . '://shop.mediavisiongroup.se',
    API_KEY,
    API_PASS, [
        'version' => 'wc/v3',
    ]
);

$map_3d = $woocommerce->get( 'products', [ 'category' => 35, 'order' => 'asc' ] );
$connect_4g = $woocommerce->get( 'products', [ 'category' => 34, 'order' => 'desc' ] );
$membership = $woocommerce->get( 'products', [ 'category' => 29, 'order' => 'desc' ] );
$currency = $woocommerce->get( 'data/currencies/current' );


/**
 * End custom product loading
 */

$available_variations = $product->get_available_variations();

$attribute_keys = array_keys( $attributes );
$variations_json = wp_json_encode( $available_variations );
$variations_attr = function_exists( 'wc_esc_json' ) ? wc_esc_json( $variations_json ) : _wp_specialchars( $variations_json, ENT_QUOTES, 'UTF-8', true );

do_action( 'woocommerce_before_add_to_cart_form' );
?>

<form class="variations_form cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data' data-product_id="<?php echo absint( $product->get_id() ); ?>" data-product_variations="<?php echo $variations_attr; // WPCS: XSS ok. ?>">
    <?php do_action( 'woocommerce_before_variations_form' ); ?>

    <?php if ( empty( $available_variations ) && false !== $available_variations ) : ?>
    <p class="stock out-of-stock">
        <?php echo esc_html( apply_filters( 'woocommerce_out_of_stock_message', __( 'This product is currently out of stock and unavailable.', 'woocommerce' ) ) ); ?>
    </p>
    <?php else : /*print_r(wc_get_attribute_taxonomies()); print_r(get_taxonomies()); print_r(get_terms('pa_color'))*/?>
    <table class="variations" id="options" cellspacing="0">
        <tr>
            <td>
                <div class="containers selection">
                    <div class="fm-5 total-price">
                        <div class="fixedAddToCart">
                            <div class="single_variation_wrap">
                                <?php
                                /**
                                 * Hook: woocommerce_before_single_variation.
                                 */
                                do_action( 'woocommerce_before_single_variation' );

                                /**
                                 * Hook: woocommerce_single_variation. Used to output the cart button and placeholder for variation data.
                                 *
                                 * @since 2.4.0
                                 * @hooked woocommerce_single_variation - 10 Empty div for variation data.
                                 * @hooked woocommerce_single_variation_add_to_cart_button - 20 Qty and cart button.
                                 */
                                do_action( 'woocommerce_single_variation' );

                                /**
                                 * Hook: woocommerce_after_single_variation.
                                 */
                                do_action( 'woocommerce_after_single_variation' );
                                ?>
                            </div>
                            <?php if(!is_user_logged_in()): ?>
                            <div class="woocommerce-variation-price"><span class="price"><a href="/my-account">Login to See Prices</a><br>request membership</span>
                            </div>
                            <?php else: ?>
                            <div class="woocommerce-variation-price"><span>Total price: <?php echo $currency->symbol ?></span>
                                <span id="price" data-price="<?php echo $product->price ?>">
                                    <?php echo $product->price ?>
                                </span>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div id="options" class="fm-6 all-options" style="float:right !important;">
                        <table>
                            <tbody>
                                <?php foreach ( $attributes as $attribute_name => $options ) : ?>
                                <tr>
                                    <td class="label">
                                        <label for="<?php echo esc_attr( sanitize_title( $attribute_name ) ); ?>">
                                            <?php echo wc_attribute_label( $attribute_name ); // WPCS: XSS ok. ?>
                                        </label>
                                    </td>
                                    <td class="value">
                                        <?php
                                        wc_dropdown_variation_attribute_options(
                                            array(
                                                'options' => $options,
                                                'attribute' => $attribute_name,
                                                'product' => $product,
                                            )
                                        );
                                        ?>
                                    </td>
                                </tr>
                                <?php endforeach; ?>

                                <!-- Begin custom product section -->
                                <tr>
                                    <td class="label"><label for="3d-map">3D map</label>
                                    </td>
                                    <td class="value">
                                        <select id="3d-map" class="" name="attribute_map" data-attribute_name="attribute_map" data-show_option_none="yes">
                                            <option value="">Choose an option</option>
                                            <option value="yes" class="attached enabled">Yes</option>
                                            <option value="no" class="attached enabled">No</option>
                                        </select>
                                        <div id="section-3d">
                                            <?php 
							foreach ($map_3d  as $attr) {
								?>
                                            <small>
                                                <?php echo strip_tags($attr->short_description); ?>
                                            </small><br>
                                            <?php
                                            if ( $attr->name == 'Startup and setup fee' ) {
                                                ?>
                                            <input type="checkbox" data-id="<?php echo $attr->id ?>" data-price="<?php echo $attr->price ?>" checked disabled/> Startup and setup fee -
                                            <?php echo $currency->symbol . $attr->price ?>
                                            <?php
                                            } else if ( $attr->name == 'Included outdoor environment' ) {
                                                ?>
                                            <input type="checkbox" data-id="<?php echo $attr->id ?>" data-price="<?php echo $attr->price ?>"/> Included outdoor environment -
                                            <?php echo $currency->symbol . $attr->price ?>
                                            <?php
                                            } else {
                                                ?>
                                            <input type="number" min=0 data-id="<?php echo $attr->id ?>" data-price="<?php echo $attr->price ?>"/>
                                            <?php echo $attr->name?> -
                                            <?php echo $currency->symbol . $attr->price ?> each
                                            <?php
                                            }
                                            }
                                            ?>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label"><label for="4g-map">4G connectivity</label>
                                    </td>
                                    <td class="value">
                                        <select id="4g-connectivity" class="" name="attribute_map" data-attribute_name="attribute_map" data-show_option_none="yes">
                                            <option value="">Choose an option</option>
                                            <option value="yes" class="attached enabled">Yes</option>
                                            <option value="no" class="attached enabled">No</option>
                                        </select>
                                        <div id="section-4g">
                                            <?php
                                            foreach ( $connect_4g as $attr ) {
                                                ?>
                                            <small>
                                                <?php echo strip_tags($attr->short_description); ?>
                                            </small><br>
                                            <?php
                                            if ( $attr->name == '4G router' ) {
                                                ?>
                                            <div class="option-classic">
                                                <span class="item-name">
											     <input type="checkbox" data-id="<?php echo $attr->id ?>" data-price="<?php echo $attr->price ?>"/> 4G router
                                                  </span>
                                            
                                                <span class="item-price">
                                                + <?php echo $currency->symbol . $attr->price ?>
                                                </span>
                                            
                                                <p class="clear"></p>
                                            </div>
                                            <?php
                                            } else {
                                                ?>
                                            <div class="option-classic">
                                                <span class="item-name">
                                              <input type="radio" name="4g" data-id="<?php echo $attr->id ?>" data-price="<?php echo $attr->price ?>" /> <?php echo $attr->name?>
                                                </span>
                                            
                                                <span class="item-price">
                                                + <?php echo $currency->symbol . $attr->price ?> / month
                                                </span>
                                            
                                                <p class="clear"></p>
                                            </div>
                                            <?php
                                            }
                                            }
                                            ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                    if ( !wc_memberships_is_user_active_member( $uid, $attr->slug ) ) {
                                ?>
                                <tr>
                                    <td class="label"><label for="membership">Membership</label>
                                    </td>
                                    <td class="value">
                                        <select id="membership" class="" name="membership" data-attribute_name="membership" data-show_option_none="yes">
                                            <option value="">Choose an option</option>
                                            <option value="yes" class="attached enabled">Yes</option>
                                            <option value="no" class="attached enabled">No</option>
                                        </select>
                                            <div id="section-membership">
                                                <?php
                                                $uid = get_current_user_id();
                                                foreach ( $membership as $attr ) {
                                                ?>
                                                        <small>
                                                            <?php echo strip_tags($attr->short_description); ?>
                                                        </small><br>
                                                        <div class="option-classic">
                                                            <span class="item-name">
                                                                <input type="radio" name="membership" data-id="<?php echo $attr->id ?>" data-price="<?php echo $attr->price ?>"/> <?php echo $attr->name?> 
                                                            </span>
                                                            <span class="item-price">
                                                                + <?php echo $currency->symbol . $attr->price ?> / month
                                                            </span>
                                                            <p class="clear"></p>
                                                        </div>
                                                        <?php
                                                    }
                                                ?>
                                            </div>
                                        <?php echo end( $attribute_keys ) === $attribute_name ? wp_kses_post( apply_filters( 'woocommerce_reset_variations_link', '<a class="reset_variations" href="#">' . esc_html__( 'Clear', 'woocommerce' ) . '</a>' ) ) : ''; ?>
                                    </td>
                                </tr>
                                <?php } else {
                                }
                                ?>
                                <!-- End custom product section -->
                            </tbody>
                        </table>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <?php endif; ?>

    <?php do_action( 'woocommerce_after_variations_form' ); ?>
</form>

<?php
do_action( 'woocommerce_after_add_to_cart_form' );