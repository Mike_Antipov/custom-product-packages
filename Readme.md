## WooCommerce product as option plugin

It should be a quick MVP of a plugin that allows to add products as options to another products using custom template and WooCommerce REST API. Since it was a WIP, there are hardcoded values, no classes etc., but you can grasp the idea.