jQuery(document).ready(function() {

    let initial_price = 0,
        total = 0,
        tmpCart = [];

    // Calculate on reload
    selectVariation();
    calculatePrice();

    // Check radio on div click
    $('#section-4g>div, #section-membership>div').mousedown(function () {
        $(this).find('input').click();
    });

    // Variation select
    $('#screen, #color, input[title="Qty"]').change(function () {
        selectVariation();
        calculatePrice();
    });

    // 3D Map show subsection
    $('#3d-map, #4g-connectivity, #membership').change(function () {
        if ($(this).val() == 'yes') {
            $(this).nextAll('div').show();
            calculatePrice();
        } else {
            $(this).nextAll('div').hide();
            calculatePrice();
        }
    });

    // Check for subsection changes
    $('#section-3d input, #section-4g input, #section-membership input').change(() => {
        calculatePrice();
    });

    // Check which variation is selected
    function selectVariation() {
        if($('#screen').val() != '' && $('#color').val() != '') {
            let screenVar = $('#screen').val(),
                colorVar = $('#color').val(),
                variations_json = JSON.parse($('.variations_form.cart').attr('data-product_variations'));

            variations_json.map((e) => {
                if(e.attributes.attribute_color == colorVar && e.attributes.attribute_screen == screenVar) {
                    initial_price = e.display_price;
                }
            });
        }
    }

    // Calculate price
    function calculatePrice() {
        let sum = 0,
            membership = 0;

        // Calculate checkboxes
        $('#section-3d input[type="checkbox"]:checked').each(function(i, e){
            $('#3d-map').val() == 'yes' ? sum += parseInt($(this).attr('data-price')) : '';
        });

        $('#section-4g input[type="checkbox"]:checked').each(function(i, e){
            $('#4g-connectivity').val() == 'yes' ?  sum += parseInt($(this).attr('data-price')) : '';
        });

        // Calculate number inputs
        $('#section-3d input[type="number"]').each(function(i, e){
            if ($('#3d-map').val() == 'yes') {
                if($(this).val() > 0) {
                    sum += parseInt($(this).attr('data-price') * $(this).val());
                }
            }
        });

        // Calculate radio inputs
        $('#section-4g input[type="radio"]:checked').each(function(i, e){
            $('#4g-connectivity').val() == 'yes' ? sum += parseInt($(this).attr('data-price')) : '';
        });

        $('#section-membership input[type="radio"]:checked').each(function(i, e){
            $('#membership').val() == 'yes' ? membership += parseInt($(this).attr('data-price')) : '';
        });

        total = initial_price + sum;
        let qty = $('input[title="Qty"]').val();

        // Miltiply quantity
        total = qty > 1 ? (total * qty) + membership : total + membership;

        $('#price').text(total);

        // Show price or login if all inputs are selected
        if ($('#screen').val() != '' && $('#color').val() != '' && $('#3d-map').val() != '' && $('#4g-connectivity').val() != '' && $('#membership').val() != '' ) {
            $('.woocommerce-variation-price').show();
        } else {
            $('.woocommerce-variation-price').hide();
        }
    }

    // Add all sub-products on 'Add to cart' submission and submit main product
    $('.single_add_to_cart_button').one('click', function(e) {
        e.preventDefault();
        // Set quantity
        let qty = $('input[title="Qty"]').val();
        $('#section-3d input, #section-4g input, #section-membership input').each(function (i, e) {
            // Check if a sub-option is set to 'yes'
            if ($(this).parent().prev().val() == 'yes') {
                // Check if a sub-option section input is selected or set
                if ($(this).prop('checked') || $(this).val() > 0) {
                    // Check if it's a number input
                    if ($(this).val() > 0) {
                        tmpCart.push({ id: $(this).attr('data-id'), qty: $(this).val() * qty });
                    } else if ($(this).attr('name') == 'membership') {
                        tmpCart.push({ id: $(this).attr('data-id'), qty: 1 });
                    } else
                        tmpCart.push({ id: $(this).attr('data-id'), qty: 1 * qty });
                }
            }
        });
        $('#section-3d input, #section-4g input, #section-membership input').promise().done(async function() {
            await addProducts(tmpCart);
        })
    });

    // Add to cart function
    async function addProducts(tmpCart) {
        $.ajax({
            type: 'POST',
            url: '/?wc-ajax=add_to_cart_batch',
            dataType: 'json',
            data: {tmpCart}
        })
        .done(function() {
            $('.variations_form.cart').submit();
        })
        .always(function() {
            $('.variations_form.cart').submit();
        });
    }

    // Sticky add to cart
    let a2c = $('.fixedAddToCart'),
        initialOffset = a2c.offset().top;
    $(window).scroll(function(e){
        let currentOffset = $(this).scrollTop(),
            tableHeight = $('.variations').height();
        if (currentOffset > initialOffset && currentOffset < initialOffset + tableHeight){ 
          a2c.css({'position': 'fixed', 'top': '120px', 'z-index': '100'});
        } else {
          a2c.css({'position': 'relative', 'top': '0px', 'z-index': '0'});
        } 
      });

    // Set active class on radios 
    $('.option-classic').click(function() { 
        $(this).parent().find('.option-selected').attr('class', 'option-classic'); 
        $(this).attr('class', 'option-selected');
    }); 
});